import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { AuthorsComponent } from './authors/authors.component';
import { PresentationComponent } from './presentation/presentation.component';
import { IntroComponent } from './intro/intro.component';
import { LoginComponent } from 'app/auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { DashboardComponent } from './panel/dashboard/dashboard.component';
import { PanelComponent } from './panel/panel.component';
import { SettingsComponent } from './panel/settings/settings.component';
import { PollsComponent } from './panel/polls/polls.component';
import { ReportsComponent } from './panel/reports/reports.component';
import { MainComponent } from './panel/main/main.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { PollEditComponent } from './panel/polls/poll-edit/poll-edit.component';
import { PollViewerComponent } from './panel/polls/poll-viewer/poll-viewer.component';

const appRoutes: Route[] = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent, children: [
        {path: '', component: IntroComponent},
        {path: 'documentation', component: DocumentationComponent},
        {path: 'authors', component: AuthorsComponent},
        {path: 'presentation', component: PresentationComponent}
    ]},
    {path: 'panel', component: PanelComponent, canActivate: [ AuthGuardService ], children: [
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent, children: [
        {path: '', redirectTo: 'main', pathMatch: 'full'},
        {path: 'main', component: MainComponent},
        {path: 'settings', component: SettingsComponent},
        {path: 'polls', component: PollsComponent},
        {path: 'polls/edit', component: PollEditComponent},
        {path: 'polls/edit/:id', component: PollEditComponent},
        {path: 'reports', component: ReportsComponent}
      ]},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
