import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { AuthorsComponent } from './authors/authors.component';
import { PresentationComponent } from './presentation/presentation.component';
import { AppRoutingModule } from './app-routing.module';
import { IntroComponent } from './intro/intro.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { DashboardComponent } from './panel/dashboard/dashboard.component';
import { PanelComponent } from './panel/panel.component';
import { PollsComponent } from './panel/polls/polls.component';
import { ReportsComponent } from './panel/reports/reports.component';
import { SettingsComponent } from './panel/settings/settings.component';
import { MainComponent } from './panel/main/main.component';
import { ReportsService } from './panel/reports/reports.service';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { PollsService } from 'app/panel/polls/polls.service';
import { PollEditComponent } from './panel/polls/poll-edit/poll-edit.component';
import { PollViewerComponent } from './panel/polls/poll-viewer/poll-viewer.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        DocumentationComponent,
        AuthorsComponent,
        PresentationComponent,
        IntroComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        PanelComponent,
        PollsComponent,
        ReportsComponent,
        SettingsComponent,
        MainComponent,
        PollEditComponent,
        PollViewerComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        ReactiveFormsModule
    ],
    providers: [ReportsService, AuthService, AuthGuardService, PollsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
