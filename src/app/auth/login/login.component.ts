import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLinkActive } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  error = {
    state: false,
    message: ''
  };

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'remember_me': new FormControl(null)
    });
  }

  onSubmit() {
    this.authService.signInUser(this.form.get('email').value, this.form.get('password').value)
      .then(
        (result) => {
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      )
      .catch(
        (error) => {
          this.error.state   = true;
          this.error.message = error.message;
        }
      );
  }

}
