import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const authRoutes = ['/panel/login', '/panel/register'];

    if ((authRoutes.indexOf(state.url) > -1) && this.authService.isAuthenticated()) {
      this.router.navigate(['/home', 'dashboard', 'main']);
      return !this.authService.isAuthenticated();
    } else if (authRoutes.indexOf(state.url) > -1) {
      return true;
    }
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    return this.authService.isAuthenticated();
  }
}
