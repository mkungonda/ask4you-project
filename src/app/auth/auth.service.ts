import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {
  token: string;
  authState: boolean               = false;
  authStateEvent: Subject<boolean> = new Subject();
  tokenUpdate: Subject<string> = new Subject();
  app: firebase.app.App;
  constructor() {
  }

  setApp() {
    this.app = firebase.initializeApp({
      apiKey: 'AIzaSyBhsrElpA_1jxEAlnqGPVz5F1A7kdVJ6Gg',
      authDomain: 'ng-ask4you.firebaseapp.com',
      databaseURL: 'https://ng-ask4you.firebaseio.com',
    });
  }

  signInUser(email: string, password: string) {
    this.authState = true;
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  signUpUser(email: string, password: string) {
    this.authState = true;
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  logoutUser() {
    firebase.auth().signOut();
    this.token     = null;
    this.authState = false;
  }

  updateAuthStatus(user) {
    this.authState = !!user;
    this.authStateEvent.next(this.authState);
    if (user) {
      this.getToken();
    }
  }

  getToken() {
    if (this.token) {
      return this.token;
    }
    firebase.auth().currentUser.getToken()
      .then(
        (token: string) => {
          this.token = token;
          this.tokenUpdate.next(token);
        }
      ).catch(
      error => console.log(error)
    );
  }

  isAuthenticated() {
    return this.authState;
  }

  getUserHash() {
    return firebase.auth().currentUser.uid;
  }
}
