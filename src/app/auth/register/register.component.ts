import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;

  static validationSame(control: FormControl): { [s: string]: boolean } {
    if ( control.root.get('password') && (control.root.get('password').value !== control.value)) {
      return {'sameAsPassword': false};
    }
    return null;
  }

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'password_repeat': new FormControl(null, [Validators.required, RegisterComponent.validationSame])
    });
  }

  onSubmit() {
    this.authService.signUpUser(this.form.get('email').value, this.form.get('password').value)
      .then(
        (response) => {
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      ).catch(
        error => console.log(error)
    );
  }
}
