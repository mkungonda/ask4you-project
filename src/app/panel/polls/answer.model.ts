export class Answer {
  constructor(
    public answer: string,
    public type: string
  ) {}
}