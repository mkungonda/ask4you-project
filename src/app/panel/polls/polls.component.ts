import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { PollsService } from './polls.service';
import { Poll } from './poll.model';
import { Subscription } from 'rxjs/Subscription';
import { default as swal } from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

declare const $: any;
declare const _: any;
declare const jsPDF: any;
@Component({
  selector: 'app-polls',
  templateUrl: './polls.component.html',
  styleUrls: ['./polls.component.css']
})
export class PollsComponent implements OnInit, OnDestroy {
  polls                = [];
  subscription: Subscription;
  loadingData: boolean = true;
  @ViewChild('button') button;

  constructor(private pollService: PollsService, private router: Router, private activeRoute: ActivatedRoute) {
  }

  ngOnInit() {
    $(this.button.nativeElement).tooltip();
    $(this.button.nativeElement).click((e) => {
      $(e.currentTarget).tooltip('hide');
    });
    this.pollService.fetchPolls();
    this.subscription = this.pollService.updated.subscribe(
      (polls: Poll[]) => {
        this.polls       = polls;
        this.loadingData = false;
      }
    );
  }

  isEmpty() {
    return _.isEmpty(this.polls);
  }

  isActive(poll) {
    return (poll.content.status === this.pollService.STATUS_ACTIVE);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  startPoll(id: number) {
    swal({
      type: 'success',
      title: 'Poll Activation',
      text: 'Your poll is now active',
      timer: 3000
    }).then(
      function () {
      },
      function (dismiss) {
        if (dismiss === 'timer') {
        }
      }
    );
    this.pollService.startPoll(id);
  }

  stopPoll(id: number) {
    swal({
      type: 'warning',
      title: 'Poll Deactivation',
      text: 'Your poll is now stopped',
      timer: 3000
    }).then(
      function () {
      },
      function (dismiss) {
      }
    );
    this.pollService.stopPoll(id);
  }

  downloadPollPdf(id: number) {
    swal({
      type: 'info',
      title: 'PDF download',
      text: 'Your download is starting!',
      timer: 3000,
      showCancelButton: false,
      showConfirmButton: false
    }).then(
      () => {
        console.log('aaaa');
      },
      (dismiss) => {
        this.generatePdf(this.polls[id]);
        if (dismiss === 'timer') {
        }
      }
    );
  }

  generatePdf(poll) {
    const doc       = new jsPDF('portrait', 'pt', 'a4'),
          content   = poll.content,
          x         = 40,
          pageLimit = 760;

    let y = 40;

    doc.setFontSize(10);
    doc.text('Made with Love by Ask4You', 40, 800);

    doc.setFontSize(25);
    doc.text(content.name, x, y);
    content.questions.forEach((questions, qi) => {
      doc.setFontSize(14);
      y += 40;
      if (this.addNewPage(pageLimit, y)) {
        doc.addPage();
        doc.setFontSize(10);
        doc.text('Made with Love by Ask4You', 40, 800);
        doc.setFontSize(14);
        y = 40;
      }

      doc.text((qi + 1) + '. ' + questions.question + ' (' + questions.type + ')', x, y);
      questions.answers.forEach((answers, ai) => {
        y += 30;
        if (this.addNewPage(pageLimit, y)) {
          doc.addPage();
          doc.setFontSize(10);
          doc.text('Made with Love by Ask4You', 40, 800);
          y = 40;
        }
        if (answers.type === 'textarea') {
          doc.setFontSize(8);
          doc.text('TIP: ' + answers.answer, x + 20, y);
          if (this.addNewPage(pageLimit, y + 100)) {
            doc.addPage();
            doc.setFontSize(10);
            doc.text('Made with Love by Ask4You', 40, 800);
            y = 40;
          }
          y += 50;
          doc.text('............................................................', x + 20, y);
          y += 50;
          doc.text('............................................................', x + 20, y);
        } else {
          doc.setFontSize(12);
          doc.text((ai + 1) + ') ' + answers.answer, x + 20, y);
        }
      });
    });

    doc.save(content.name + '.pdf');
  }

  addNewPage(height: number, current: number) {
    return (current >= height);
  }

  canEdit(id: number) {
    return (this.polls[id].content.status === 'stopped');
  }

  canDelete(id: number) {
    return (this.polls[id].content.status === 'stopped');
  }

  editPoll(id: number) {
    this.router.navigate(['edit', id], {relativeTo: this.activeRoute});
  }

  duplicatePoll(id: number) {
    this.pollService.duplicatePoll(id);
  }

  deletePoll(id: number) {
    swal({
      title: 'Delete this pool?',
      text: 'You wont\'t get it back!',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success mr-1',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(() => {
      this.pollService.deletePoll(id);
      swal({
        type: 'success',
        title: 'Poll Deleted',
        text: 'Your poll is now gone ;(',
        timer: 3000
      }).then(() => {

        },
        function (dismiss) {
        }
      );
    }, function (dismiss) {
      swal({
        type: 'error',
        title: 'Poll Saved',
        text: 'Your poll was saved!',
        timer: 3000
      }).then(
        function () {
        },
        function () {
        }
      );
    });
  }
}
