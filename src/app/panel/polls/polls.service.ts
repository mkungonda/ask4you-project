import { Injectable, OnInit } from '@angular/core';
import { Poll } from './poll.model';
import { AuthService } from '../../auth/auth.service';
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import * as firebase from 'firebase';

@Injectable()
export class PollsService {
  token: string;

  polls                 = [];
  updated: Subject<any> = new Subject<any>();

  STATUS_ACTIVE         = 'active';
  STATUS_STOPPED        = 'stopped';

  constructor(private authService: AuthService) {
    this.token = this.authService.getToken();
    this.authService.tokenUpdate.subscribe(
      (token: string) => this.token = token,
      (error) => console.log(error)
    );
  }

  storePoll(poll: Poll) {
    firebase.database().ref('polls/' + this.authService.getUserHash()).push(poll);
  }

  updatePoll (id: number, poll: Poll) {
    const oldPoll = this.getPoll(id);
    firebase.database().ref('polls/' + this.authService.getUserHash() + '/' + oldPoll.key).set(poll);
    this.refreshPolls();
  }

  duplicatePoll (id: number) {
    const poll = this.getPoll(id);
    firebase.database().ref('polls/' + this.authService.getUserHash()).push(poll.content);
    this.refreshPolls();
  }

  getPoll(id: number) {
    return this.polls[id];
  }

  fetchPolls() {
    const query = firebase.database().ref('polls/' + this.authService.getUserHash()).orderByKey();
    query.once('value').then((snapshot) => {
      this.polls = [];
      let iteration = 0;
      snapshot.forEach((childSnapshot) => {
        iteration++;
        this.polls.push({key: childSnapshot.key, content: childSnapshot.val()});
        this.updated.next(this.polls);
      });
      if (iteration < 1) {
        this.updated.next(this.polls);
      }
    });
  }

  startPoll (id: number) {
    const poll = this.polls[id];
    poll.content.status = this.STATUS_ACTIVE;

    firebase.database().ref('polls/' + this.authService.getUserHash() + '/' + poll.key).set(poll.content);
    this.refreshPolls();
  }

  stopPoll(id: number) {
    const poll = this.polls[id];
    poll.content.status = this.STATUS_STOPPED;
    firebase.database().ref('polls/' + this.authService.getUserHash() + '/' + poll.key).set(poll.content);

    this.refreshPolls();
  }

  deletePoll(id: number) {
    const poll = this.polls[id];

    firebase.database().ref('polls/' + this.authService.getUserHash() + '/' + poll.key).remove();
    this.refreshPolls();
  }

  refreshPolls() {
    this.fetchPolls();
  }
}
