import { Answer } from './answer.model';

export class Question {
  constructor(
    public question: string,
    public type: string,
    public answers: Answer[]
  ) {}
}
