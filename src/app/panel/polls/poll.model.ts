import { Question } from './question.model';

export class Poll {
  constructor(
    public name: string,
    public status: string,
    public questions: Question[],
    public updated_at: number,
    public created_at: number
  ) {}
}
