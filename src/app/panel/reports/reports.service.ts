import { Report } from './report.model';
export class ReportsService {
  private reports: Report[] = [];

  getReports() {
    return this.reports.slice();
  }
}