export class Report {
  constructor(
    public id: number,
    public poll_id: number,
    public name: string,
    public download_link: string,
    public created_at: string,
    public updated_at: string
  ) {};
}
