import { Component, OnInit } from '@angular/core';
import { ReportsService } from './reports.service';
import { Report } from './report.model';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  reports: Report[];

  constructor(private reportsService: ReportsService) { }

  ngOnInit() {
    this.reports = this.reportsService.getReports();
  }



}
