import { Ask4youProjectWwPage } from './app.po';

describe('ask4you-project-ww App', () => {
  let page: Ask4youProjectWwPage;

  beforeEach(() => {
    page = new Ask4youProjectWwPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
